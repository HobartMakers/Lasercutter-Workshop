# Laser Cutter Workshop

A workshop to introduce the LaserCutter to Hobart Makers Members and train
them on safety.

## Workshop Materials (what is in this repo)

The directories in this repo are fairly self-explanatory:

* notes/ - Static site for workshop information and notes, generated with Sphinx using
  ```make html``` and hosted on Gitlab Pages. PDF version with ```make latexpdf```
