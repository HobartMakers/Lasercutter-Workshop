==========================
The LoPy Development Board
==========================

The LoPy development board is an ESP32 microcontroller running a port of MicroPython. The ESP32 is a new
powerful dual core micro that include both 2.4GHz 802.11b/g/n wifi and BlueTooth 4.1. It also has a
Semtech LoRa transceiver SX1272.

The LoPy board also has Bluetooth but we are not exploring that in this workshop. Normally you use BlueTooth to
pair two devices together to communicate, however recently [BLUE2017]_ a Bluetooth Mesh specification was announced
which could make Bluetooth more useful as another IoT network (for a future workshop!)

The port of Micropython to the ESP32 is not complete and some features, mainly in the ``machine`` module are not
available. The ESP32 is only six months old, but it was chosen here because it has many powerful features.

You can also run other languages on the ESP32, such as Lua, JavaScript, Arduino/C/C++.

For this workshop we will introduce the basics the LoPy board in Part 1

1. Connecting your LoPy board via serial and Telnet
2. Hello World!
3. Uploading code to the LoPy

Later, if there is time or you need something more advanced, come back to Part 2 which has more code examples.

The official LoPy documentation can be found here: https://docs.pycom.io/

This workshop will use some of that documentation and examples.

Part 1 - LoPy Basics
====================

Using the MicroPython REPL
--------------------------

The REPL allows you to interactively type in Python code and see it execute in front of your eyes!

Connecting by USB Serial
^^^^^^^^^^^^^^^^^^^^^^^^

* Download and install Putty or your favourite terminal emulator
* (Windows) Start -> Device Manager
  * Search for COM Ports and find the COM port that appears when you plug in the device (eg COM15)
* (OSX) Look in the System Info app to make sure the device has been detected, your comm port should be /dev/cu.serial
  * For more information on serial ports on OSX - https://pbxbook.com/other/mac-tty.html
* (Linux) Serial ports are /dev/ttyUSBx or /dev/ttyAMAx
* Connect the LoPy board to your computer.
* Set up Putty, set to Serial connection, baud rate 115200, and the COM port you found above

Connecting by Telnet
^^^^^^^^^^^^^^^^^^^^

* Open putty and set to telnet
* Enter 192.168.4.1 or the IP address you know the device is set to
* Use username 'micro' and password 'python' to login.

Obligatory Hello World!
-----------------------

These examples are short enough to type into the REPL

Blinking a LED
^^^^^^^^^^^^^^

The expansion board LED is connected to P9/G16

.. code-block:: python

    from machine import Pin
    import time

    led_pin = Pin('P9', Pin.OUT)

    while True:
        led_pin.toggle()
        time.sleep_ms(500)

Blinking the RGB LED
^^^^^^^^^^^^^^^^^^^^

The RGB LED is connected to P2. By default it blinks in a heartbeat but this can be disabled and reused.

.. code-block:: python

    import pycom
    import time

    pycom.heartbeat(False)

    # Cycle through colours
    while True:
        pycom.rgbled(0x7f0000) # red
        time.sleep(3)
        pycom.rgbled(0x007f00) # green
        time.sleep(3)
        pycom.rgbled(0x7f7f00) # yellow
        time.sleep(3)
        pycom.rgbled(0x00007f) # blue
        time.sleep(3)


Uploading Code to the LoPy Chip
-------------------------------

The LoPy micro runs an FTP server. The username is 'micro' and the password is 'python'. If you upload your
code to the ``/flash`` directory and reset, the next boot the LoPy will run your code.

The micro executes two scripts if they exist. First boot.py which is useful to connect to WiFi or set up radios,
and then main.py which should contain your main program.

You can have additional source files in the same directory and ``import`` them.

Look at the code in example Code1-TTN-With-Config. You will see these files as well as some others.

Open the config.py file in your text editor. The config file in its current configuration is set up for running
the three sensors we will set up in the next part of the workshop, but it is also missing our LoRaWAN TheThingsNetwork
configuration.

It is often useful to load configuration variables as another python script. For example, in the main.py script
this config.py is loaded like so:

.. code-block:: python

    from config import settings

    print('Temperature sensor is connected to {}'.format(settings.dht11_pin))

What to do When Something Goes Wrong
------------------------------------

Serial debugging
^^^^^^^^^^^^^^^^

Connect the USB cable to your computer and run your favourite terminal emulator (Putty or Minicom). Set the
baud rate to 115200, 8 data bits, no parity, 1 stop bit, no flow control. This allows you to run the REPL and also see
any low-level firmware errors that might happen.

Reset in Safe Boot
^^^^^^^^^^^^^^^^^^

If your code does not work but you cannot get a telnet or FTP session to change it, then you need to use
safe boot mode. In this mode, neither the **boot.py** or **main.py** is executed and the micro runs a wifi access
point that you can connect to, and also serial comms via the USB interface.

Attach a wire between

* the 3.3v pin (3V3 on the expansion board, 3rd pin from the top on the right side)
* the P12 pin (the G20 pin on the expansion board, bottom pin on the left side)

Connect the Micro-USB cable, or press the reset button. Disconnect one of the pins before 3 seconds.

Depending how long you keep the pins connected depends what safe boot mode is used:

* 1-3 sec: safe boot with latest firmware
* more than 7 sec: safe boot with factory firmware


Part 2 - Coding for the LoPy
============================

Writing Code for the LoPy
-------------------------

While most of the Python standard library is missing, as you can see from the Pycom docs at https://docs.pycom.io
some versions of useful standard libraries exist:

* sys
* os
* math
* binascii
* json
* re
* time
* socket

WiFi
----

A brand new Lopy board is by default set up in Access Point mode - if you search for wifi networks you will
see ``lopy-XXXX``.

The LoPy board can act as both a wifi client and as an access point. Normally when writing code I like to put in
the following code in ``boot.py`` so that it joins my home network and I can access it on the same network as the
Internet.

.. code-block:: python

    import os
    import machine
    from network import WLAN

    # Replicates the REPL on both telnet and serial
    uart = machine.UART(0, 115200)
    os.dupterm(uart)

    wlan = WLAN(mode=WLAN.STA)

    my_nets = {
        'YOURSSID': 'wifipassword',
        'HobartMakers': 'makeallthethings',
    }

    found_net = None
    found_sec = None
    nets = wlan.scan()
    for net in nets:
        if net.ssid in my_nets.keys():
            found_net = net.ssid
            found_sec = net.sec
            break

    print('Connecting to %s' % found_net)
    wlan.connect(found_net, auth=(found_sec, my_nets[found_net]), timeout=5000)
    while not wlan.isconnected():
        machine.idle()
        print('Network connected');
        break


Using the LoRa as a Two-Way Radio
---------------------------------

.. code-block:: python

    from network import LoRa
    import socket
    import machine
    import time

    # initialize LoRa in LORA mode
    lora = LoRa(mode=LoRa.LORA)
    # lora.init(mode=LoRa.LORA, tx_power=14, sf=12)   # set specific power and spreading factor

    # create a raw LoRa socket
    s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

    while True:
        # send some data
        s.setblocking(True)
        s.send('Hello')

        # get any data received...
        s.setblocking(False)
        data = s.recv(64)
        print(data)

        # wait a random amount of time
        time.sleep(machine.rng() & 0x0F)

Using LoRaWAN
-------------

With the above example you can create your own group of LoRa devices that listen for all traffic and only respond
to receive events they are interested in. For example you could create your own garage door opener with the LoRa radio.

However the real power of LoRa is the ability to run as a network and you need a LoRaWAN stack for that. The LoPy
has a ready to go LoRaWAN stack that can run as a normal device or a gateway.

Creating a TTN LoRaWAN Device
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Especially with the single-channel gateway it is more successful to connect using ABP mode instead of OTAA

.. code-block:: python

    from network import LoRa
    import socket, binascii, struct

    freq = 917000000

    # Initialize LoRa in LORAWAN mode.
    lora = LoRa(mode=LoRa.LORAWAN)

    #Setup the single channel for connection to the gateway at it's known frequency
    for channel in range(0, 72):
       lora.remove_channel(channel)
    for chan in range(0, 8):
       lora.add_channel(chan,  frequency=freq,  dr_min=0,  dr_max=4)


    # create an ABP authentication params
    # These settings can be found at your TTN Console for your application/device
    dev_addr = struct.unpack(">l", binascii.unhexlify('00 00 00 05'))[0]
    nwk_swkey = binascii.unhexlify('2B 7E 15 16 28 AE D2 A6 AB F7 15 88 09 CF 4F 3C')
    app_swkey = binascii.unhexlify('2B 7E 15 16 28 AE D2 A6 AB F7 15 88 09 CF 4F 3C')

    # join a network using ABP (Activation By Personalization)
    lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))

    # create a LoRa socket
    s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

    # set the LoRaWAN data rate
    s.setsockopt(socket.SOL_LORA, socket.SO_DR, 4)

    while True:
        # Send some bytes
        s.setblocking(True)
        s.send('temperature=25'.encode('utf-8'))
        s.setblocking(False)

        # Receive some bytes
        data = s.recv(64)
        print(data.decode('utf-8'))

        # wait a random amount of time
        time.sleep(machine.rng() & 0x0F)


Creating a TTN LoRaWAN Gateway
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    import config
    from nanogateway import NanoGateway


    nanogw = NanoGateway(id=config.GATEWAY_ID, frequency=config.LORA_FREQUENCY,
                    datarate=config.LORA_DR, ssid=config.WIFI_SSID,
                    password=config.WIFI_PASS, server=config.SERVER,
                    port=config.PORT, ntp=config.NTP, ntp_period=config.NTP_PERIOD_S)

    nanogw.start()

The above code uses settings stored in config.py:

.. code-block:: python

    GATEWAY_ID = '240ac4ffff008970'

    SERVER = 'bridge.asia-se.thethings.network'
    PORT = 1700

    NTP = "pool.ntp.org"
    NTP_PERIOD_S = 3600

    WIFI_SSID = 'HobartMakers'
    WIFI_PASS = 'makeallthethings'

    LORA_FREQUENCY = 917000000
    LORA_DR = "SF7BW125"

Upgrading the Firmware
----------------------

The ESP32 port of Micropython is relatively new and constantly improving, so you may want to upgrade the firmware every
couple of months.

This page has the firmware update tool for Windows, OSX and Linux: https://docs.pycom.io/chapter/gettingstarted/installation/firmwaretool.html

It is a simple wizard that tells you to connect a wire between G23 and GND and then it will look for a LoPy board,
download and flash new firmware to the device.


.. rubric:: Footnotes

.. [BLUE2017] https://www.bluetooth.com/specifications/mesh-specifications
