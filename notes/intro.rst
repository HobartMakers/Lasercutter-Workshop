Introduction
============

.. role:: strike

Welcome to our first attempt at :strike:`cramming as many buzzwords into a workshop as possible` introducing
the public to wireless networks for sensor devices and in particular the open source The Things Network solution
using LoRaWAN.

During the workshop you will connect up some sensors to a microcontroller, upload some code to it and
then configure your account on TheThingsNetwork to receive the data.

The microcontroller code is written in MicroPython. Python is a really easy language to learn and read so you
are encouraged to look at the other code examples in this workshop. The kit used in the workshop can be used
with or without the LoRa radio and so if you are keen to learn more Python, keep an eye out for future
MicroPython workshops where we will focus on the language more than IoT/Wireless technologies.


* 18:30 - Welcome and Introduction to LoRa and IoT
* 18:45 - The Things Network
* 19:00 - Introduction to the LoPy boards
* 19:30 - Break
* 19:45 - Connecting Sensors
* 20:15 - Registering Devices on TTN and Receiving Data
* 20:30 - Doing Something With The Data
* 21:00 - Finish Up
