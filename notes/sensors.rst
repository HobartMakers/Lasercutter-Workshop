Hooking Up and Configuring Sensors
==================================

In this workshop you will hook up one of the sensors and then connect this data stream to The Things Network. You
could connect all three since they use different pins, but they all use the 3.3v and GND lines and there is only one
connection point for that.

The DHT11 Temperature/Humidity Sensor
-------------------------------------

The DHT11 can measure temperature and relative humidity. It uses a custom one-wire protocol so just requires a
single data pin.

.. only:: latex

   .. image:: _static/images/dht11-connections.png
      :height: 400px
      :alt: DHT11 connections

.. only:: html

   .. image:: _static/images/dht11-connections.svg
      :height: 400px
      :alt: DHT11 connections

I recommend plugging the data pin into G10 which is right next to the 3.3v pin. Load the Code2-TemperatureSensor
example code into your LoPy and reset it. If you are monitoring the terminal window the output should look like this:

.. image:: _static/screenshots/terminal-dht11-example.png
   :alt: DHT11 terminal

The HRC-04 Ultrasonic Distance Sensor
-------------------------------------

This sensor sends an ultrasonic pulse and waits for the return echo.

.. only:: latex

   .. image:: _static/images/UltrasonicTiming.png
      :height: 400px
      :alt: Ultrasonic timing diagram

.. only:: html

   .. image:: _static/images/UltrasonicTiming.svg
      :height: 400px
      :alt: Ultrasonic timing diagram


The time measured between the trigger and echo pulses is the time sound takes to reach the object and return. The speed
of sound is 34.3cm per millisecond, so you can calculate the distance to the object by:

.. math::

      distance = elapsed \times 34.3 / 2

You need to connect the sensor trigger and echo to the P11 and P12 pins while the power needs to go to the 3.3v pin
Do not use Vin pin as this is 5v and will make the trigger and echo pins also 5v which will kill those pins on the
board

.. only:: latex

   .. image:: _static/images/ultrasonic-connections.png
      :height: 400px
      :alt: Ultrasonic connections

.. only:: html

   .. image:: _static/images/ultrasonic-connections.svg
      :height: 400px
      :alt: Ultrasonic connections


The DFRobot Soil Moisture Sensor
--------------------------------

The soil moisture detects galvanic changes caused by the change of conductivity of the water in the soil. This
sensor is not very reliable. For example dry soil may measure 82% of full ADC, and wet soil would be 95%. It is best
that this sensor measures that a "wetting" event has occurred rather than an actual calibrated value.

Any of the connections from P13 to P20 can be used. The P20 pin is shown connected here.

.. only:: latex

   .. image:: _static/images/soilmoisture-connections.png
      :height: 400px
      :alt: Soil moisture connections

.. only:: html

   .. image:: _static/images/soilmoisture-connections.svg
      :height: 400px
      :alt: Soil moisture connections

Once you stick the sensor in the soil and pour a little water, you will see the ADC value change:

.. image:: _static/screenshots/terminal-soil-example.png
   :alt: Soil Moisture terminal

Reference: LoPy Pinout
----------------------

Larger PDF can be found at http://faulteh.gitlab.io/LoRa-IoT-Workshop/_static/lopy_pinout.pdf

   .. image:: _static/images/lopy_pinout.png
      :width: 650px
      :alt: LoPy Pinout

Reference: Expansion Board Pinout
---------------------------------

Larger PDF can be found at http://faulteh.gitlab.io/LoRa-IoT-Workshop/_static/expansion_v02_pinout.pdf

   .. image:: _static/images/expansion_v02_pinout.png
      :width: 650px
      :alt: Expansion Board Pinout
