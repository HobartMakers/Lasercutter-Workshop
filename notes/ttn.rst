The Things Network
==================

The Things Network (**TTN**) is an *open source* and *open access* LoRaWAN network.

Benefits:
 * If there is a gateway in range of your devices you can use TTN.
 * Your application has its own encryption key.
 * Internet infrastructure already exists to collect your data
 * There are some existing integrations to use your data with other services

Other IoT Networks
------------------

These are similar to TTN, but are either not free, have limited free options or are
not open source:

* ThingSpeak_ - Servers for data acquisition from different IoT devices
* Loriot_ - Global LoRaWAN commercial network services company
* NNNCo_ - Australian Commercial company - National Narrowband Network

And other some are based on other technologies:

* SigFox_ - Both a technology competing with LoRaWAN, and a cloud service.
* NB-IOT_ - another narrowbank technology using LTE/Cellular resource blocks

.. _ThingSpeak: https://thingspeak.com/
.. _Loriot: https://loriot.io/
.. _NNNCo: https://www.nnnco.com.au/
.. _SigFox: https://www.sigfox.com/en
.. _NB-IOT: https://en.wikipedia.org/wiki/NarrowBand_IOT

Running a Gateway
-----------------

You can become a gateway operator like Hobart Makers by setting up your own gateway
and participating in the network and allowing others to use your gateway
without restriction. This increases coverage of TTN network.

Since LoRa is not a high data rate it does not take a huge amount of your Internet
connection's bandwidth or monthly quota.

Connecting to TTN
-----------------

During the workshop you will be doing:

1. Create a TTN account
2. Join the Hobart TTN Community
3. Create a TTN application
4. Register a device
5. Receive Data
6. Decode Data
7. Set up an Integration for Received Data

If you get through all of this and want to continue discovering TTN features:

8. Writing an application for your data
9. Set up an Integration for Sending Data
10. Encode Data
11. Set up your controller to receive data

Create a TTN Account
^^^^^^^^^^^^^^^^^^^^

Open ``https://www.thethingsnetwork.org/`` in your browser and click the
sign up button near the top.

You may need to verify your email address before going to the next step.

Join the Hobart TTN Community
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After you have created your account, login and go to the Hobart Community page:

``https://www.thethingsnetwork.org/community/hobart/``

* Click the Join Community button near the top of the page.
* Optionally subscribe to the TTN Newsletter
* When you do something or make something with your LoRaWAN devices, create a blog post on
  the community page to share it with all of us!

Create a TTN application
^^^^^^^^^^^^^^^^^^^^^^^^

Go to the console and you will see two buttons - Applications and Gateways. Click on Applications.

Click the Add Application button.

The application name should be lower case with no spaces. Select the asia-se router.

.. image:: _static/screenshots/ttn-app-new.png
   :alt: New application


Register a device
^^^^^^^^^^^^^^^^^

Now it is time to add your device to your network. We are going to configure it with ABP (Activation By Personalization)
rather than OTAA which I have found hit-and-miss on this single channel gateway. When the multi-channel gateway gets set
up you should be able to use OTAA.

Click on register a new device.

We need to find out the Device EUI for your LoRa device. This is like the Ethernet MAC address.
You can get this by typing some commands on the REPL:

.. code-block:: python

   from network import LoRa
   import binascii
   lora = LoRa(mode=LoRa.LORAWAN)
   binascii.hexlify(lora.mac())

Copy/Paste this string into the Device EUI and give the device a name you can recognise later.

After adding the device you now need to go to the device settings.

* Change from OTAA to ABP
* Enter a human readable name for the device if it is blank

.. image:: _static/screenshots/ttn-device-edit.png
   :alt: Edit Device

Receive Data
^^^^^^^^^^^^

If your LoPy device is running and it is connected to the network you should be able to click
on the Data tab of the application and start seeing packets. You can then click on the packets to get
more metadata. As you can see the payload is a series of bytes, so not quite useful for us yet.

.. image:: _static/screenshots/app-data-packet.png
   :alt: Raw data


Decode Data
^^^^^^^^^^^

In the decoder section you need to write a tiny bit of JavaScript to turn the payload which is an array
of bytes into a JSON object that we can feed to other applications

.. code-block:: javascript

    function Decoder(bytes, port) {
      // Decode an uplink message from a buffer
      var decoded = {};
      // convert payload to string from array of bytes
      var result = "";
        for(var i = 0; i < bytes.length; ++i){
            result+= (String.fromCharCode(bytes[i]));
        }
      var result_split = result.split('=');
      if (result_split[1] === 'None') { result_split[1] = null; }
      decoded[result_split[0]] = result_split[1];
      return decoded;
    }

Afterwards, you can go back to the Data panel for the application and you will see the payload packets, but
now there will be an annotation next to them with the decoded data:

.. image:: _static/screenshots/app-data-decoded.png
   :alt: Decoded data

Set up an Integration for Received Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the application page on TTN, you need to create a key for the integration. Click the Manage key button and add
a new key, call it http_integration or something like that. Back on the application page you will see the new key
listed. Copy the key and keep it somewhere.

Click on the integraionts tab and click Add Integration. Select Data storage and add the integration.

It is ready to go! Click on the 'go to platform' link and you will be given an API request screen which you can
now send queries to.

To test this out, click on the Authorise button and paste in the key you saved before.

.. image:: _static/screenshots/swagger-query.png
   :alt: Swagger Query


Writing an application for your data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that you know the key you can use this to query the data storage engine directly.

In the ``Code5-DataStorage-Integration`` directory is an example web page that queries the Data storage API
and produces a graph for the last hour of data.

You will need to modify the page and add your API key and URL. The URL will be https://scotts_sensors.data.thethingsnetwork.org/api/v2/query
but you need to replace ``scotts_sensors`` with the name of your application.

You can also create an HTTP endpoint integration where each received packet will be sent to a web application you write
but that is a more advanced topic, perhaps to be reviewed at a later workshop on Python web development!

